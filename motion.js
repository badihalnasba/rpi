var config=require('./config.json');
var sms=require('./SMSGateway.js');
var stop=config.motiondetector.stoppins;//[false,false,false,false,false];
var pins=config.motiondetector.pins;//["","26","19","13","6"];
var buzzertime=config.motiondetector.alarmtime*1000;
var buzzerpin=config.motiondetector.buzzerpin;
var detected =false;
module.exports={
start:function(num){
    if(num=="all"){
  var Gpio = require('onoff').Gpio,
 buzzer = new Gpio(buzzerpin, 'out'),
  pir1 = new Gpio(pins[1], 'in', 'both'), pir2 = new Gpio(pins[2], 'in', 'both'), pir3 = new Gpio(pins[3], 'in', 'both'), pir4 = new Gpio(pins[4], 'in', 'both');
pir1.watch(function(err, value) {
if(stop[1]) return;
  if (err){ return;} 
 if(value==1 && !detected){
      buzzer.writeSync(1);
detected=true;
  console.log("Intruder detected at motion detector 1");
setTimeout(()=>{ //function to stop blinking
  buzzer.writeSync(0); // Turn LED off
detected=false;   }, buzzertime);
sms.send("Intruder detected at motion detector 1" );
}
});
pir2.watch(function(err, value) {
  if(stop[2]) return;
    if (err){ return;} 
   if(value==1 && !detected){
      buzzer.writeSync(1);
detected=true;
    console.log("Intruder detected at motion detector 2");
  setTimeout(()=>{ //function to stop blinking
    buzzer.writeSync(0); // Turn LED off
 detected=false;    }, buzzertime);
  sms.send("Intruder detected at motion detector 2" );
  }
  });
  pir3.watch(function(err, value) {
    if(stop[3]) return;
      if (err){ return;} 
    if(value==1 && !detected){
      buzzer.writeSync(1);
detected=true;
      console.log("Intruder detected at motion detector 3");
    setTimeout(()=>{ //function to stop blinking
      buzzer.writeSync(0); // Turn LED off
    detected=false;   }, buzzertime);
    sms.send("Intruder detected at motion detector 3" );
    }
    });
    pir4.watch(function(err, value) {
      if(stop[4]) return;
        if (err){ return;} 
      if(value==1 && !detected){
      buzzer.writeSync(1);
detected=true;
        console.log("Intruder detected at motion detector 4");
      setTimeout(()=>{ //function to stop blinking
        buzzer.writeSync(0); // Turn LED off
detected=false;       
}, buzzertime);
      sms.send("Intruder detected at motion detector 4" );
      }
      });
  }
  if (pins[num]){stop[num]=false; console.log("start motion detector "+num);}
},
stop:function(num){
  if (pins[num]){stop[num]=true; console.log("stop motion detector "+num);}
},
stopall:function(){
    stop[1]=true; console.log("stop motion detector "+1);
    stop[2]=true; console.log("stop motion detector "+2);
    stop[3]=true; console.log("stop motion detector "+3);
    stop[4]=true; console.log("stop motion detector "+4);
},
startall:function(){
    stop[1]=false; console.log("start motion detector "+1);
    stop[2]=false; console.log("start motion detector "+2);
    stop[3]=false; console.log("start motion detector "+3);
    stop[4]=false; console.log("start motion detector "+4);
},
status:function(num,callback){
callback(stop);
}
};
