var config=require('./config.json');
var http = require('http').createServer(handler); 
var fs = require('fs'); 
var request = require('request');
var sms=require('./SMSGateway.js');
var totaltime=0.00;
var offtimer=setTimeout(() => {console.log("GenServer started")},0);																	
fs.readFile("./t.tim",function(err,data){
 if (!err) totaltime=parseFloat(data.toString());
});
//var Gpio = require('onoff').Gpio,on = new Gpio(4, 'out'),marche = new Gpio(27, 'out');
var Gpio = require('onoff').Gpio,on = new Gpio(config.GenONOFFServer.onpin, 'out'),marche = new Gpio(config.GenONOFFServer.marchepin, 'out');
var url=require('url');
var timeAgo = require('node-time-ago');
var ontime=120;
var repeatTimes = config.GenONOFFServer.repeatTimes;//3;
var nbtry=0;
var trytoturnon=false;
var marcheTime=config.GenONOFFServer.marcheTime;//2;
var Lastoffdate=new Date();
var Lastondate=new Date();
var VoltageReadHost=config.GenONOFFServer.VoltageReadHost;//'http://localhost:81';
http.listen(config.GenONOFFServer.serverPort);// 82
function handler (req, res) { //create server
    if (req.method === 'POST') {
        var data = '';
    
        req.on('data', function(chunk) {
          data += chunk;
        });
    
        req.on('end', function() {
          // parse the data
         // DOIT(req,data);
    if (url.parse(req.url).pathname=="/on")  
    {    console.log(data);
      return  TurnON(res,req,data.replace('time=',''));
    }
   else  if  (url.parse(req.url).pathname=="/off")
              return TurnOFF(res,req);
         else 
         if  (url.parse(req.url).pathname=="/reset"){
          fs.writeFile("./t.tim",0, function(err) {
            if(err) {
                return console.log(err);
            }
            fs.appendFile("./total.tim",'Reseted At '+(new Date())+' total time='+((totaltime+((new Date()).getTime()-Lastondate.getTime())/60000)/60).toFixed(2)+' hours\n', function(err) {
              if(err) {console.log(err);
                return  res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>ERROR in reset<br /> Please don't refresh the page</body></html>")
    
              }
               });
        totaltime=0.00;
         return  res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>The Total time is reseted<br /> Please don't refresh the page</body></html>")
         
        }); 

         }
         else 
         return  res.end('error:UNKNOWN Post');
        });
      }
else
{
    request(VoltageReadHost+'/V1', function (error, response, body) {
        if (error){
            console.log('error:', error); // Print the error if one occurred
          return  res.end('error:'+error);
        }
        
        if(response.statusCode==200){
         console.log('Voltage server:', body);  
        if (body.indexOf("OFF") !== -1){
            fs.readFile(__dirname + '/public/GenOFF.html', function(err, data) { //read file index.html in public folder
                if (err) {
                  res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
                  return res.end("404 Not Found");
                } 
            
                res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML

                res.write(data.toString().replace('OFFdate',timeAgo(Lastoffdate)).replace('totaltime',(totaltime/60).toFixed(2) + ' hours')); 
                
                return res.end();
              });

        }
      else  if (body.indexOf("ON") !== -1){
            fs.readFile(__dirname + '/public/GenON.html', function(err, data) { //read file index.html in public folder
                if (err) {
                  res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
                  return res.end("404 Not Found");
                } 
                  
                res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
            
                res.write(data.toString().replace('ondate',timeAgo(Lastondate)).replace('ontime',ontime + ' Mins').replace('totaltime',((totaltime+((new Date()).getTime()-Lastondate.getTime())/60000)/60).toFixed(2) + ' hours')); 
                               return res.end();
              });

        }
     
        else  return  res.end('error:UNKNOWN generator status');
        } 
        else  return  res.end('error:bad response');
       
      });
  
}
}
 
 function TurnON(res,req,time) {
    try{
      if(trytoturnon) return;
      console.log("try to turn on");
      ontime=time;
      trytoturnon=true;
console.log("try to turn on for : "+time);
      
        request(VoltageReadHost+'/V1', function (error, response, body) {
          if (error){
              console.log('error:', error); // Print the error if one occurred
            return  res.end('error:'+error);
          }
          
          if(response.statusCode==200){
             
          if (body.indexOf("OFF") !== -1){
				console.log("ON write 1")					   
            on.writeSync(1);
            setTimeout(() => {marcheon(res,req)},1000);

          }
        else  if (body.indexOf("ON") !== -1){
                    console.log("already on");
            trytoturnon=false;   							
          res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>The Generator is already ON <br /> Please don't refresh the page</body></html>")
            
          }
       
          else  return  res.end('error:UNKNOWN generator status');
          } 
          else  return  res.end('error:bad response');
         
        });



   
    }
    catch(err)
    {
      res.end('error:'+err);
      //res.end('Turned ON For '+ time + ' mins');
    }
    
    }
   
    function marcheon(res,req){
		console.log("marche=1");							
      marche.writeSync(1);
      setTimeout(() => {marcheoff(res,req)},marcheTime*1000);
    }
    function marcheoff(res,req){
		console.log("marche=0");							
     marche.writeSync(0);
   setTimeout(() => {ChechGen(res,req)}, 1000); 
   
    }
function ChechGen(res,req){ request(VoltageReadHost+'/V1', function (error, response, body) {
  if (error){
	console.log("check gen error");										 
    on.writeSync(0);
   marche.writeSync(0);
   nbtry=0;
   trytoturnon=false;
      console.log('error:', error); // Print the error if one occurred
    return  res.end("Cannot turn on the generator<br />"+'error:'+error);
  }
  
  if(response.statusCode==200){
     
  if (body.indexOf("OFF") !== -1){
		 console.log("check gen: still off after a try");															
        if(nbtry===repeatTimes)
        {console.log("check gen: end all try");
   on.writeSync(0);
   marche.writeSync(0);
   nbtry=0;
   trytoturnon=false;
   return res.end("Cannot turn on the generator");
      } 
      console.log('number of try:'+nbtry)
      nbtry++;
      setTimeout(() => {marcheon(res,req)},1000);

  }
else  if (body.indexOf("ON") !== -1){
      console.log("check gen: gen is on set off time"+ontime); 
   marche.writeSync(0);
   nbtry=0;
   trytoturnon=false;
   Lastondate=new Date();
    clearTimeout(offtimer);
     offtimer=setTimeout(() => {TurnOFF()},ontime*1000*60);
  
   sms.send("Generator is turned ON");
   res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>The Generator is turned ON <br /> Please don't refresh the page</body></html>")
   
  }

  else {console.log("check gen :unkown status");
   on.writeSync(0);
   marche.writeSync(0);
   nbtry=0;
   trytoturnon=false;return  res.end('error:UNKNOWN generator status');} 
  } 
  else {
	  console.log("check gen :bad response");									  
   on.writeSync(0);
   marche.writeSync(0);
   nbtry=0;
   trytoturnon=false;return  res.end('error:bad response');} 
 
});
}
  function TurnOFF(res,req) {
        Lastoffdate=new Date();
        on.writeSync(0);
       marche.writeSync(0);
        nbtry=0;
       trytoturnon=false;
      if(res)  res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>The Generator is turned OFF<br /> Please don't refresh the page</body></html>")
      fs.writeFile("./t.tim",(totaltime+((Lastoffdate.getTime()-Lastondate.getTime())/60000)).toFixed(2), function(err) {
        if(err) {
            sms.send("Generator is turned OFF\nerror in update of total time");
            return console.log(err);
            
        }
    totaltime=totaltime+(Lastoffdate.getTime()-Lastondate.getTime())/60000;
    sms.send("Generator is turned OFF\nTotal time ="+(totaltime/60).toFixed(2)+" hours" );
console.log("Generator is turned OFF\nTotal time ="+(totaltime/60).toFixed(2)+" hours" );
    }); 
         }
function exit() {
on.writeSync(0);  
marche.writeSync(0);  
on.unexport();
marche.unexport();
process.exit();
}

process.on('SIGINT', exit); 
