var five= require('johnny-five');
var config=require('./config.json');
var http = require('http').createServer(handler); 
var fs = require('fs');  
var url=require('url');
var sms=require('./SMSGateway.js');
var request=require('request');
var A0='not calculated yet';
var A1 ='not calculated yet';
var A2 ='not calculated yet';
var LowVoltage=config.VoltageServer.LowVoltage;//10.5;
var time=config.VoltageServer.ontime;//60;
var commandsent=false;
var waitingForResponse=false;
var smsnotsent=false;
var GenServerHost=config.VoltageServer.GenServerHost;//'http://localhost:82';
var board = new five.Board({repl: false});
http.listen(config.VoltageServer.serverPort);//81 
function handler (req, res) { 
   if (url.parse(req.url).pathname=="/V0")
  return res.end(A0.replace('V', ''));
  if (url.parse(req.url).pathname=="/V1")
  return res.end(A1);

  if (url.parse(req.url).pathname=="/V2")
  return res.end(A2);
    fs.readFile(__dirname + '/public/readVoltage.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    }        
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data.toString().replace('A0', A0).replace('A1', A1).replace('A2', A2) ); //write data from index.html
    return res.end();
  });
}
board.on("ready", function() {
var A0Sensor = new five.Sensor("A0");
var A1Sensor = new five.Sensor("A1");
var A2Sensor = new five.Sensor("A2");
  // Scale the sensor's data from 0-1023 to 0-10 and log changes
  A0Sensor.on("change", function() {
    A0=this.fscaleTo(0, 24).toFixed(2)+'V';
    if (this.fscaleTo(0,24)<LowVoltage){
     if(!commandsent){
       
      if(!waitingForResponse){
        waitingForResponse=true;
      request.post({
      url:  GenServerHost+'/on',
      method: "POST",
      json: false,   
      body: "time="+time
	
  }, function (error, response, body) {
         waitingForResponse=false;
         smsnotsent=true;
            if (!error) {
                              console.log("Turned on command sent :"+body)
                              if (A1.indexOf("OFF") !==-1){commandsent=false;}
            }

        }
    );
    console.log("Voltage is very low");
sms.send("Voltage is very low");
        }  
    
    commandsent=true; 

  }
   
  }
    else {
      if (this.fscaleTo(0,24)<(LowVoltage+0.5)){
         if(smsnotsent){
       console.log("Voltage is low");
       sms.send("Voltage is low");
       smsnotsent=false;
      }
          
     }


    if (A1.indexOf("OFF") !==-1){commandsent=false;}
          }
  });
  A1Sensor.on("change", function() {
    if (this.fscaleTo(0,24) > 0.5) {A1='ON:'+this.fscaleTo(0, 24).toFixed(2) +'V';commandsent=true;}
    else 
   { A1='OFF:'+this.fscaleTo(0, 24).toFixed(2) +'V';}
  });
  A2Sensor.on("change", function() {
   A2=this.fscaleTo(0, 25).toFixed(2) +'V';
    });
});