var five= require('johnny-five');
var config=require('./config.json');
var http = require('http').createServer(handler); 
var fs = require('fs');  
var url=require('url');
var sms=require('./SMSGateway.js');
var request=require('request');
var A0='not calculated yet';
var A1 ='not calculated yet';
var LowVoltage=config.VoltageServer.LowVoltage;//10.5;
var time=config.VoltageServer.ontime;//60;
var commandsent=false;
var waitingForResponse=false;
var GenServerHost=config.VoltageServer.GenServerHost;//'http://localhost:82';
var board = new five.Board({repl: false});
http.listen(config.VoltageServer.serverPort);//81 
function handler (req, res) { 
    if (url.parse(req.url).pathname=="/try") setInterval(tryvoltage, 1000);
    if (url.parse(req.url).pathname=="/0V0")
   {A0='0V';   return res.end(A0.replace('V', ''));}
   if (url.parse(req.url).pathname=="/1V0")
   {A0='12V';   return res.end(A0.replace('V', ''));}
   if (url.parse(req.url).pathname=="/0V1")
   {A1='OFF';   return res.end(A1.replace('V', ''));}
   if (url.parse(req.url).pathname=="/1V1")
   {A1='ON'; commandsent=true;  return res.end(A1.replace('V', ''));}
   if (url.parse(req.url).pathname=="/V0")
  return res.end(A0.replace('V', ''));
  if (url.parse(req.url).pathname=="/V1")
  return res.end(A1);
    fs.readFile(__dirname + '/public/readVoltage.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    }        
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data.toString().replace('A0', A0).replace('A1', A1) ); //write data from index.html
    return res.end();
  });
}
function tryvoltage () {

     if(!commandsent){
       
      if(!waitingForResponse){
        waitingForResponse=true;
      request.post({
      url:  GenServerHost+'/on',
      method: "POST",
      json: false,   
      body: "time="+time
  }, function (error, response, body) {
         waitingForResponse=false;
            if (!error) {
                              console.log("Turned on command sent :"+body)
                              if (A1.indexOf("OFF") !==-1){commandsent=false;}
            }

        }
    );
    console.log("Voltage is low");
sms.send("Voltage is low");
        }  
    
    commandsent=true; 

  }  
    else {
    if (A1.indexOf("OFF") !==-1){commandsent=false;}
          }
   
 
} 
