var http = require('http').createServer(handler);
var config=require('./config.json');
var fs = require('fs');
var url = require('url');
var motiodetector= require('./motion.js');
motiodetector.start("all");
http.listen(config.motiondetector.serverPort); //84
function handler (req, res) {
  
  if (req.method === 'POST') {
    var data = '';

    req.on('data', function(chunk) {
      data += chunk;
    });

    req.on('end', function() {
     
if (url.parse(req.url).pathname=="/on")  
{   
 motiodetector.start(data.replace('pin=',''));
 return res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>Motion detector on "+data+" is started <br /> Please don't refresh the page</body></html>")
           
}
else if (url.parse(req.url).pathname=="/onall")  
{   
 motiodetector.startall();
 return res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>All Motion detectors are started <br /> Please don't refresh the page</body></html>")
           
}
else if (url.parse(req.url).pathname=="/offall")  
{   
 motiodetector.stopall();
 return res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>All Motion detectors are stoped <br /> Please don't refresh the page</body></html>")
           
}
else  if  (url.parse(req.url).pathname=="/off")
          {
            motiodetector.stop(data.replace('pin=',''));
            return res.end("<html><head><meta http-equiv=\"Refresh\" content=\"3\"; url=\""+req.headers.host+"\"></head><body>Motion detector on "+data+" is stoped <br /> Please don't refresh the page</body></html>")
            
        }
     else 
     return  res.end('error:UNKNOWN Post');
    });
  }
  else
  fs.readFile(__dirname + '/public/motion.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    } 
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
motiodetector.status(0,function(status){

     res.write(data.toString().replace('pin1', !status[1]).replace('pin2',!status[2]).replace('pin3', !status[3]).replace('pin4', !status[4]) ); //write data from index.html
    return res.end();
});
   
  });
}
