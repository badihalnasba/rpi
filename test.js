var five= require('johnny-five')
var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var url=require('url');
var A0='not calculated yet';
var A1 ='not calculated yet';
var sms=require('./SMSGateway.js');
var board = new five.Board({repl: false});
var offtimer=setTimeout(() => {console.log("GenServer started")},0);



http.listen(81); 
function handler (req, res) { 
 console.log(url.parse(req.url));
  if (url.parse(req.url).pathname=="/V0")
  return res.end(A0.replace('V', ''));
  if (url.parse(req.url).pathname=="/V1")
  return res.end(A1);
    fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
      
    } 
       
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    clearTimeout(offtimer);
      offtimer=setTimeout(() => {console.log("GenServer started")},0);
      sms.send("Test");
    res.write(data.toString().replace('A0', A0).replace('A1', A1) ); //write data from index.html
    return res.end();
  });
}



function init(){
  board.on("ready", function() {
var A0Sensor = new five.Sensor("A0");
var A1Sensor = new five.Sensor("A1");
  // Scale the sensor's data from 0-1023 to 0-10 and log changes
  A0Sensor.on("change", function() {
    console.log(A0);
    A0=this.fscaleTo(0.00, 26.00)+'V';
  });

  A1Sensor.on("change", function() {
    console.log(A0);

    if (this.fscaleTo(0, 26) > 1) A1='ON:'+this.fscaleTo(0, 26) +'V';
    else 
    A1='OFF:'+this.fscaleTo(0, 26) +'V';
  });
});
}
exports.init = init;
