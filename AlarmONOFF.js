var http = require('http').createServer(handler); 
var fs = require('fs'); 
var request = require('request');
var sms=require('./SMSGateway.js');
var config=require('./config.json');
var pin =config.AlarmONOFFServer.pin;//10;
var AlarmState="ON";
var Gpio = require('onoff').Gpio,on = new Gpio(pin, 'out');
var url=require('url');
var Lastoffdate=new Date(2000,1,1,1,2,2);
var Lastondate=new Date();
var timeAgo = require('node-time-ago');
http.listen(config.AlarmONOFFServer.serverPort); //85
function handler (req, res) { 
    if (req.method === 'POST') {
        var data = '';
    
        req.on('data', function(chunk) {
          data += chunk;
        });
    
        req.on('end', function() {
          
    if (url.parse(req.url).pathname=="/on")  
    {    
      return  TurnON(res,req,data.replace('time=',''));
    }
   else  if  (url.parse(req.url).pathname=="/off")
              return TurnOFF(res,req);
         else 
         return  res.end('error:UNKNOWN Post');
        });
      }
else
{
            if (AlarmState.indexOf("OFF") !== -1){
            fs.readFile(__dirname + '/public/AlarmON.html', function(err, data) { //read file index.html in public folder
                if (err) {
                  res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
                  return res.end("404 Not Found");
                } 
            
                res.writeHead(200, {'Content-Type': 'text/html'}); 
            
                res.write(data.toString().replace('OFFdate',timeAgo(Lastoffdate)).replace('ontime',ontime + ' Mins')); 
                
                return res.end();
              });

        }
      else  if (AlarmState.indexOf("ON") !== -1){
            fs.readFile(__dirname + '/public/AlarmOFF.html', function(err, data) { 
                if (err) {
                  res.writeHead(404, {'Content-Type': 'text/html'}); 
                  return res.end("404 Not Found");
                } 
            
                res.writeHead(200, {'Content-Type': 'text/html'});
            
                res.write(data.toString().replace('ondate',timeAgo(Lastondate))); 
                
                return res.end();
              });

        }
     
        else  return  res.end('error:UNKNOWN Alarm status');
       
  
}
}

 function TurnON(res,req,time) {
    try{ ontime=time;
        sms.send("Alarm is ON");
AlarmState="OFF";
Lastoffdate=new Date();
           on.writeSync(1);
           res.end("<html><head><meta http-equiv=\"Refresh\" content=\"2\"; url=\""+req.headers.host+"\"></head><body>"+'Alarm will be turned  OFF after '+time+' Mins'+"<br /> Please don't refresh the page</body></html>");
setTimeout(() => {
    TurnOFF();
    
}, time*1000*60);

   
    }
    catch(err)
    {
      res.end('error:'+err);
      
    }
    
    }
   
    
      

  function TurnOFF(res,req) {
    Lastondate=new Date();
        sms.send("Alarm is OFF");
        on.writeSync(0);
        AlarmState="ON";
      if(res)  res.end("<html><head><meta http-equiv=\"Refresh\" content=\"2\"; url=\""+req.headers.host+"\"></head><body>"+'Alarm is turned  OFF'+"<br /> Please don't refresh the page</body></html>");     }
function exit() {
on.writeSync(0);  
on.unexport();
process.exit();
}

process.on('SIGINT', exit); 
